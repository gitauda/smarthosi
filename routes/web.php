<?php


use App\Http\Controllers\CoreLogs;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin Dashboard
Route::get('dashboard','CoreMains@index');

//Login
Route::get('/admin', 'CoreLogs@index')->name('admin'); //Login Page
Route::match(['get','post'],'admin/{value}', ['uses' =>'CoreLogs@valid'])->name('admin/valid'); //Login Verification
// Route::get('admin/open/{value}', ['uses' =>'CoreLogs@open'])->name('admin/open/'); //Login Verification
